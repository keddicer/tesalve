$(document).ready(function(){

    /* ------------------------------------------- user menu */

    /* mobile */
    if ($(window).width() < 767) {

        $('.user-menu').click(function(){

            var userMenuItemsHeight = 0;

            $('.user-menu li').each(function() {
               userMenuItemsHeight += $(this).height();
            });
            if ($('.user-menu').css('height')=='70px') {
                $('.user-menu').css('height', userMenuItemsHeight);
            }
            else {
                $('.user-menu').css('height', 70);
            }
        });

    }
    /* other devices */
    else {

        $('.user-menu').hover(function(){

            var userMenuItemsHeight = 0;

            $('.user-menu li').each(function() {
               userMenuItemsHeight += $(this).height();
            });
            if ($('.user-menu').css('height')=='70px') {
                $('.user-menu').css('height', userMenuItemsHeight);
            }
            else {
                $('.user-menu').css('height', 70);
            }
        });

    }

    /* ------------------------------------------- lateral menu */

    function documentHeight() {
        var lateralHeight = ($(document).height())-70;
        $('.lateral-menu .bck').css('height', lateralHeight);
    };
    documentHeight();

    /* ------------------------------------------- ellipsis content */

    function ellipsisContent() {

        $('.list td a').each(function() {

          var listContentWidth = $(this).text().length;

          if (listContentWidth > 130) {
              $(this).addClass('ellipsis-patch');
          }
        });

    };
    ellipsisContent();

    /* ------------------------------------------- table list accordion */

    $('.list td .icon-folder').on('click', function() {

        var capturaId = $(this).parent().attr("id");
        var lateralMenuHeight = $('.lateral-menu .bck').height();
        var tableHeight = $('#'+capturaId).children('.list').height();

        if ($('#'+capturaId).children('.list').css('display')=='none') {
            $(this).css('opacity', '1');
            $(this).next().css('color', '#252525');
            $(this).removeClass('icon-folder');
            $(this).addClass('icon-folder-alt');
            if ($(window).width() > 910) {
                $('#'+capturaId).children('.list').css('display', 'table');
            }
            else {
                $('#'+capturaId).children('.list').css('display', 'block');
            }
            documentHeight();
        }
        else {
            $(this).css('opacity', '0.3');
            $(this).next().css('color', '#959595');
            $(this).removeClass('icon-folder-alt');
            $(this).addClass('icon-folder');
            $('#'+capturaId).children('.list').css('display', 'none');
            $('.lateral-menu .bck').css('height', lateralMenuHeight-tableHeight);
        }
    });

    $('.topics .list td .icon-folder').on('click', function() {

        var capturaId = $(this).parent().parent().attr("id");
        var tableWidth = ($('#'+capturaId).width())-20;

        if ($('#'+capturaId+'+tr>td').children('.list').css('display')=='none') {
            $(this).css('opacity', '1');
            $(this).removeClass('icon-folder');
            $(this).addClass('icon-folder-alt');
            $('#'+capturaId+'+tr').css('background', '#f8f8f8');
            $('#'+capturaId+'+tr>td .list tr').css('background', '#f8f8f8');
            if ($(window).width() > 910) {
                $('#'+capturaId+'+tr>td').children('.list').css('display', 'table');
                $('#'+capturaId+'+tr>td').children('.list').css('width', tableWidth);
            }
            else {
                $('#'+capturaId+'+tr>td').children('.list').css('display', 'block');
                $('#'+capturaId+'+tr>td').children('.list').css('width', tableWidth);
            }
        }
        else {
            $(this).css('opacity', '0.3');
            $(this).next().css('color', '#959595');
            $(this).removeClass('icon-folder-alt');
            $(this).addClass('icon-folder');
            $('#'+capturaId+'+tr>td').children('.list').css('display', 'none');
        }
    });

    /* ------------------------------------------- answers */

    $('.answers-list label').click(function(){

        var capturaClass = $(this).attr("class");

        if (capturaClass=='unselected') {
            $('.answers-list label').removeClass('selected');
            $(this).addClass('selected');
        }

    });

    /* ------------------------------------------- popup */

    function closePopUp() {
        $('.popup').css('display','none');
    }

    $('.popup .focus').click(function(){ closePopUp(); });
    $('.popup .content .icon-close').click(function(){ closePopUp(); });

    /* ------------------------------------------- table actions */

    $('.right-actions').on('click', function() {

        var capturaId = $(this).attr("id");

        function closeOtherActions() {
            $('.right-actions .icon-settings').css({'transform':'rotate(0deg)','opacity':'.3'});
            $('.right-actions .icon-trash').css({'right':'-30px','opacity':'0','width':'0'})
            $('.right-actions .icon-pencil').css({'right':'-20px','opacity':'0','width':'0'})
            $('.right-actions .icon-plus').css({'right':'-10px','opacity':'0','width':'0','margin-right':'0'})
            $('.right-actions .bck').css({'left':'115px','right':'25px','opacity':'0','overflow':'hidden'})
        };

        if ($('#'+capturaId).children('.icon-pencil').css('opacity')=='0') {
            closeOtherActions();
            $('#'+capturaId).children('.icon-settings').css({'transform':'rotate(-180deg)','opacity':'1'});
            $('#'+capturaId).children('.icon-trash').css({'right':'0','opacity':'1','width':'30px'})
            $('#'+capturaId).children('.icon-pencil').css({'right':'0','opacity':'1','width':'30px'})
            $('#'+capturaId).children('.icon-plus').css({'right':'0','opacity':'1','width':'30px','margin-right':'3px'})
            $('#'+capturaId).children('.bck').css({'left':'0','right':'35px','opacity':'.8','overflow':'inherit'})
        }
        else {
            $('#'+capturaId).children('.icon-settings').css({'transform':'rotate(0deg)','opacity':'.3'});
            $('#'+capturaId).children('.icon-trash').css({'right':'-30px','opacity':'0','width':'0'})
            $('#'+capturaId).children('.icon-pencil').css({'right':'-20px','opacity':'0','width':'0'})
            $('#'+capturaId).children('.icon-plus').css({'right':'-10px','opacity':'0','width':'0','margin-right':'0'})
            $('#'+capturaId).children('.bck').css({'left':'115px','right':'25px','opacity':'0','overflow':'hidden'})
        }
    });

    /* ------------------------------------------- topics select */

    /* open list */
    $('.topic-selection .select').click(function(){

        if ($('.topic-selection').css('height')=='40px') {
            $('.topic-selection').css('height', '280px');
            $('.select .icon').css('transform', 'rotate(180deg)');
        }
        else {
            $('.topic-selection').css('height', '40px');
            $('.select .icon').css('transform', 'rotate(0deg)');
        }
    });
    /* open list */
    $('.topics input[type="checkbox"]').click(function(){

        var capturaId = $(this).attr('id');
        var capturaNombre = $('#'+capturaId).next('.name').text();

        if ($(this).prop('checked')) {

            if ($('.select .empty').css('display')=='inline-block') {
                $('.select .empty').remove();
            }
            $('.select').append('<span class="'+capturaId+'">'+capturaNombre+'</span>');

        }
        else {

            $('.select > .'+capturaId+'').remove();

            var selectNombre = $('.select').text();

            if (selectNombre=='') {
                $('.select').append('<span class="empty">Seleccione algún tema</span>');
            }
        }

    });

    /* ------------------------------------------- trash



    */

});
