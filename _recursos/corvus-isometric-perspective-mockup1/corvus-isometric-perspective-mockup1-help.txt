Hey there! Thanks for downloading Corvus Design Studio's Isometric Perspective Mockup.

Key features:

- file resolution: 3000x2200.
- changeable background.
 

This mockup uses smart objects for easy editing. Smart object editing is easy:

1. Open corvus-isometric-perspective-mockup1.psd and find the layer(s) named "IMAGE 1", "IMAGE 2", "IMAGE 3". You may notice that these layers are turned off. That's ok, just leave them that way.
2. Double click the smart object thumbnail (the little icon displayed on the layer itself). Another file will open.
3. Paste your artwork into the new file and edit as needed. Save the file. 
4. Go back to the mockup PSD and you'll see your artwork displayed.
5. In the folder "background" you will find a vector object that conrols the background color. 


If you have any problems please email Corvus Design Studio from our website: www.corvusart.com/contact-corvus-design-studio.php

Please let us know what you think of this mockup in the comments section of: www.corvusart.com/blog/free-isometric-perspective-website-mockup

If you like it, please share it!