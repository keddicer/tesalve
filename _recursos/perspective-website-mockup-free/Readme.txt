Using our presentation mockups is really simple. You just need to follow these 3 simple steps:

1. Open the PSD file
2. Go to layers and double-click the Smart Object layer
3. Replace the Sample with Your design and Save it. (Match the colors and size of sample graphic with your graphic to get best results)

And you are all done. Congratulations!

For any help regarding this file, please feel free to contact us on Support@zippypixels.com